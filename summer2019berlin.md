---
layout: default
title: CS4641  - Schedule
---

# Summer 2019 Berlin

- Lectures: Mondays, Wednesdays and Thursdays, 13:00 - 14:30

## Lecture Schedule

  Topics link to slides, where available.  Topics noted with (Recitation) are covered in recitation that week, not in lecture. You're responsible for everything listed on this schedule, some of which may not be covered in lecture or recitation.


- LfD means [Learning from Data](http://amlbook.com/)
- AIML means [An Introduction to Machine Learning](https://www.springer.com/us/book/9783319639123)
- I2ML means [Introduction to Machine Learning](https://mitpress.mit.edu/books/introduction-machine-learning-third-edition)
- RL means [Reinforcement Learning: An Introduction](http://incompleteideas.net/book/the-book.html)
- ISL means [An Introduction to Statistical Learning](https://www-bcf.usc.edu/~gareth/ISL/)
- PRML means [Pattern Recognition and Machine Learning](https://www.micr.com/en-us/research/people/cmbishop/#!prml-book)
- ESL means [The Elemets of Statistical Learning](https://web.stanford.edu/~hastie/ElemStatLearn/)
- NND means [Neural Network Design](http://hagan.okstate.edu/nnd.html)
- MML means [Mathematics for Machine Learning](https://mml-book.com/)
- VMLS means [Introduction to Applied Linear Algebra -- Vectors, Matrices and Least Squares](http://vmls.stanford.edu/)
- PSCS means [Probability and Statistics for Computer Science](https://www.spr.com/us/book/9783319644097#otherversion=9783319644103)


<div class="table-responsive">
  <table class="table table-bordered table-hover">

    <tr class="table-header active">
      <th>Date</th>
      <th>Topics</th>
      <th>Reading and Resources</th>
      <th>Reminders</th>
    </tr>
    
    
    <tr class="active"><td colspan="4">Week 1</td></tr>
    
    
    
    <tr>
      <td>2019-05-13</td>
      <td>
        
        Orientation
      </td>
      <td>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-15</td>
      <td>
        
        <a href="slides/cs4641-intro.pdf">Intro to CS4641</a>
      </td>
      <td>
        
        <a href="resources.html">Getting Started</a><br/>
        
        <a href="https://www.artima.com/shop/programming_in_scala_3ed">Programming in Scala</a>, Ch 1, 2<br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-16</td>
      <td>
        
        <a href="slides/intro-ml.pdf">Intro to Machine Learning</a>
      </td>
      <td>
        
        Primary reading: LfD, Ch 1.1, 1.2<br/>
        
        Supplemental reading: ISL, Ch 1, 2.1<br/>
        
        Deeper reading: PRML 1<br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 2</td></tr>
    
    
    
    <tr>
      <td>2019-05-20</td>
      <td>
        
        Scipy
      </td>
      <td>
        
        <a href="slides/numpy.pdf">Numpy</a><br/>
        
        <a href="slides/pandas.pdf">Pandas</a><br/>
        
        <a href="slides/matplotlib.pdf">Matplotlib</a><br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-22</td>
      <td>
        
        <a href="slides/pla.pdf">Perceptron Learning Algorithm</a>
      </td>
      <td>
        
        Primary reading: LfD, 1.1.2<br/>
        
        Supplemental reading: <br/>
        
        Math background: VMLS, 1, 2<br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw0/hw0-spark.html">HW0 Spark Released</a><br/>
        
        <a href="summer2019berlin/hw0/hw0-scipy.html">HW0 Scipy Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-23</td>
      <td>
        
        <a href="slides/error.pdf">Error</a><br/>
        
        learnability
      </td>
      <td>
        
        Primary reading: LfD 1.4, ISL 2.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 3</td></tr>
    
    
    
    <tr>
      <td>2019-05-27</td>
      <td>
        
        <a href="slides/error.pdf">Error</a>
      </td>
      <td>
        
        Primary reading: LfD 1.4, ISL 2.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        HW0 Due 2019-05-28
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-29</td>
      <td>
        
        Spark
      </td>
      <td>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-05-30</td>
      <td>
        
        Himmelfahrt - No Class
      </td>
      <td>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 4</td></tr>
    
    
    
    <tr>
      <td>2019-06-03</td>
      <td>
        
        <a href="slides/linear-classification.pdf">Linear Classification</a>
      </td>
      <td>
        
        Primary reading: LfD 3.1<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw1/ml-problems.html">HW 1 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-05</td>
      <td>
        
        <a href="slides/linear-regression.pdf">Linear Regression</a>
      </td>
      <td>
        
        Primary reading: LfD 3.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-06</td>
      <td>
        
        <a href="slides/logistic-regression.pdf">Logistic Regression</a>
      </td>
      <td>
        
        Primary reading: LfD 3.3<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect09.pdf">Logistic regression slides</a><br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 5</td></tr>
    
    
    
    <tr>
      <td>2019-06-10</td>
      <td>
        
        Pfingsten - No Class
      </td>
      <td>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/prj1/fingerprints.html">Project 1 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-12</td>
      <td>
        
        <a href="slides/nonlinear-transformation.pdf">Nonlinear Transformation</a>
      </td>
      <td>
        
        Primary reading: LfD 3.4<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect10.pdf">Nonlinear Transformation slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw1/ml-problems.html">HW 1 Due</a><br/>
        
        <a href="summer2019berlin/hw2/linear-model.html">HW2 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-13</td>
      <td>
        
        <a href="slides/ml-theory.pdf">Computational Learning Theory</a>
      </td>
      <td>
        
        Primary reading: LfD 1.3, 1.4.2, 2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 6</td></tr>
    
    
    
    <tr>
      <td>2019-06-17</td>
      <td>
        
        <a href="slides/ml-theory.pdf">Computational Learning Theory</a>
      </td>
      <td>
        
        Primary reading: LfD 1.3, 1.4.2, 2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw2/learning-practice.html">HW4 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-19</td>
      <td>
        
        <a href="slides/ml-practice.pdf">Machine Learning Practice</a>
      </td>
      <td>
        
        Primary reading: LfD 4.1, 4.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect11.pdf">Overfitting slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect12.pdf">Regularization slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect13.pdf">Validation slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect14.pdf">Learning Principles slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw2/linear-model.html">HW2 Due</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-20</td>
      <td>
        
        <a href="slides/ml-practice.pdf">Machine Learning Practice</a>
      </td>
      <td>
        
        Primary reading: LfD 4.1, 4.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect11.pdf">Overfitting slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect12.pdf">Regularization slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect13.pdf">Validation slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect14.pdf">Learning Principles slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw3/theory-practice.html">HW3 Released</a>
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 7</td></tr>
    
    
    
    <tr>
      <td>2019-06-24</td>
      <td>
        
        <a href="slides/ml-practice.pdf">Machine Learning Practice</a>
      </td>
      <td>
        
        Primary reading: LfD 4.1, 4.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect11.pdf">Overfitting slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect12.pdf">Regularization slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect13.pdf">Validation slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect14.pdf">Learning Principles slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/prj1/fingerprints.html">Project 1 Due 2019-06-25</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-26</td>
      <td>
        
        <a href="slides/review1.pdf">Review 1</a>
      </td>
      <td>
        
        Primary reading: <br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect15.pdf">Review1 slides</a><br/>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-06-27</td>
      <td>
        
        <a href="slides/exam1.pdf">Exam 1</a>
      </td>
      <td>
        
        Primary reading: <br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/cs4641-summer2019berlin-exam1-answers.pdf">Answers</a>
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 8</td></tr>
    
    
    
    <tr>
      <td>2019-07-01</td>
      <td>
        
        <a href="slides/decision-trees.pdf">Decision Trees</a><br/>
        
        <a href="slides/combining-multiple-learners.pdf">Ensemble Methods</a>
      </td>
      <td>
        
        Primary reading: AIML 6<br/>
        
        Supplemental reading: I2ML 9<br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Byron Boots <a href="https://www.cc.gatech.edu/~bboots3/CS4641-Fall2018/Lecture2/02_DecisionTrees.pdf">Decision Trees slides</a><br/>
        
        Primary reading: AIML 9.1-9.3<br/>
        
        Supplemental reading: I2ML 17<br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Alpaydin <a href="https://www.cmpe.boun.edu.tr/~ethem/i2ml3e/3e_v1-0/i2ml3e-chap17.pdf">Combining Learners slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/prj2/lenses.html">Project 2 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-07-03</td>
      <td>
        
        <a href="slides/knn.pdf">K-Nearest Neighbors</a><br/>
        
        <a href="slides/rbf.pdf">Radial Basis Functions (k-Means Clustering)</a>
      </td>
      <td>
        
        Primary reading: LfD 6.1 - 6.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect16.pdf">Nearest Neighbor slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect17.pdf">Nearest Neighbor efficiency slides</a><br/>
        
        Primary reading: LfD 6.3<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect18.pdf">Radial Basis Function slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw4/decision-trees.pdf">HW4 Released</a> (<a href="summer2019berlin/hw4/decision-trees-answers.pdf">HW4 answers</a>)
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-07-04</td>
      <td>
        
        In-Class Exercise
      </td>
      <td>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw5/similarity-based-methods.pdf">HW5 Released</a> (<a href="summer2019berlin/hw5/similarity-based-methods-answers.pdf">HW5 answers</a>)
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 9</td></tr>
    
    
    
    <tr>
      <td>2019-07-08</td>
      <td>
        
        <a href="slides/density-estimation.pdf">Probability Density Estimation (k-Means, GMM, EM)</a>
      </td>
      <td>
        
        Primary reading: <br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect19.pdf">Unsupervised Learning slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/prj3/churn.html">Project 3 Released</a>
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-07-10</td>
      <td>
        
        <a href="slides/pca.pdf">Input Processing</a><br/>
        
        <a href="slides/svm.pdf">Support Vector Machines</a>
      </td>
      <td>
        
        Primary reading: LfD 9.2<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect27.pdf">Learning Aides slides</a><br/>
        
        Primary reading: LfD 8<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect23.pdf">SVM: Maximizing Margin slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect24.pdf">Optimal Hyperplane Overfitting slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect25.pdf">Kernel Trick slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect26.pdf">Kernel Machines slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw6/svm-neural-networks.pdf">HW6 Released</a> (<a href="summer2019berlin/hw6/svm-neural-networks-answers.pdf">HW6 answers</a>)
      </td>
    </tr>
    
    
    
    <tr>
      <td>2019-07-11</td>
      <td>
        
        <a href="slides/neural-networks.pdf">Neural Networks</a>
      </td>
      <td>
        
        Primary reading: 7.1-7.5<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect20.pdf">Multilayer Perceptron slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect21.pdf">Backpropagation slides</a><br/>
        
        Magdon <a href="http://www.cs.rpi.edu/~magdon/courses/LFD-Slides/SlidesLect22.pdf">Neural Network Overfitting slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/prj2/lenses.html">Project 2 Due</a>
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Week 10</td></tr>
    
    
    
    <tr>
      <td>2019-07-15</td>
      <td>
        
        <a href="slides/mdp.pdf">Markov Decision Processes</a><br/>
        
        <a href="slides/q-learning.pdf">Q Learning</a>
      </td>
      <td>
        
        Primary reading: RL 1-4<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Byron Boots <a href="https://www.cc.gatech.edu/~bboots3/CS4641-Fall2018/Lecture18/18_MDPs1.pdf">MDP1 slides</a><br/>
        
        Byron Boots <a href="https://www.cc.gatech.edu/~bboots3/CS4641-Fall2018/Lecture19/19_MDPs2.pdf">MDP2 slides</a><br/>
        
        Primary reading: RL 5<br/>
        
        Supplemental reading: <br/>
        
        Deeper reading: <br/>
        
        Math background: <br/>
        
        Byron Boots <a href="https://www.cc.gatech.edu/~bboots3/CS4641-Fall2018/Lecture20/20_RL1.pdf">RL1 slides</a><br/>
        
        Byron Boots <a href="https://www.cc.gatech.edu/~bboots3/CS4641-Fall2018/Lecture21/21_RL2.pdf">RL1 slides</a><br/>
        
        
      </td>
      <td>
        
        <a href="summer2019berlin/hw7/reinforcement-learning.pdf">HW7 Released</a> (<a href="summer2019berlin/hw7/reinforcement-learning-answers.pdf">HW7 answers</a>)
      </td>
    </tr>
    
    
    
    <tr class="active"><td colspan="4">Final Exam</td></tr>
    
    
    
    <tr>
      <td>2019-07-20</td>
      <td>
        
        11:00 - 13:50
      </td>
      <td>
        
        
      </td>
      <td>
        
        
      </td>
    </tr>
    
    
  </table>
</div>
