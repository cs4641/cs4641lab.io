---
layout: homework
title: Linear Model
---

# Linear Model

- For each of the following, state whether you would use binary classification, multi-class classification, or regression and give a one or two sentence justification. 

    - Given a mobile phone customer's account age, monthly bill, usage data and other factors, predict the likelihood that the customer will switch to another provider at the end of their current contract.
    - Given ages, household income, and other factors, predict the number of children a couple will have.
    - Given a traveler's answers to a series of questions, decide whether the the traveler is allowed across the bridge or not.
    - Given a college graduate's undergraduate major, institution, GPA, major GPA and other factors, predict their salary.

- What are the general steps of most learning algorithms?

- What is the difference between online learning algorithms and bath learning algorithms?

- How can binary classfiers be used to solve a multiclass classification problem?

- How can linear models deal with data sets that are not linearly separable due to noise in the data?

- What is the form of the hypothesis class of linear classifiers?

- What are the functional forms and the loss functions for

    - the perceptron learning algorithm,
    - linear regression, and
    - logistic regression.

- What is a local minimum?

- What is a global minimum?

- What is the meaning of the learning rate hyperparameter to a learning algorithm?

- What may happen if the learning rate is set too low?

- What may happen if the learning rate is set too high?

- Describe two desirable features of the sigmoid loss function for logistic regression.

- What is a feature transform?

- Can a linear model be used to separate into classes the feature vectors of instances that are not linearly separable?  If so, how?

- What happens to sample complexity (the number of training samples we need to maintain a bound our generalization error) with higher-order polynomial transforms?

## Turn-in Procedure

Write up your homework in a format that can be converted to a PDF file (like LaTeX) and name the PDF file `linear-model.pdf`.  Submit your `linear-model.pdf` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
