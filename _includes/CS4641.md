# CS4641 Machine Learning

## Instructor

- Christopher Simpkins, simpkins@cc.gatech.edu

**IMPORTANT**:

- You must email me from your official Georgia Tech email address, that is, the email address listed for you in the official course roster in Canvas.  I use your official email address to create email filters.  
- Include context in your email -- which class you're in, etc.
- Include all of your IDs, e.g., 9-digit GTID, Buzzport login ID, official name.  Every system at GT uses a different ID and if I can't easily look up relevant information about your request because you didn't give me enough identifying information, I may ignore your email.
- Please understand that professors are slow or unresponsive to email because we are drowning in email.  If I don't respond within 48 hours just send me a gentle reminder.
- Do not send me messages in Canvas.

## Course Description

CS4641 is an introductory survey of modern machine learning. Machine learning is an active and growing field that would require many courses to cover completely. This course aims at the middle of the theoretical versus practical spectrum. We will learn the concepts behind several machine learning algorithms wtihout going deeply into the mathematics and gain practical experience applying them. We will consider pattern recognition and artificial intelligence perspectives, making the course valuable to students interested in data science, engineering, and intelligent agent applications.

## Learning Outcomes

- Know a broad survey of approaches and techniques in machine learning
- Understand the major general ideas in machine learning
- Programming skills that will help you to build intelligent, adaptive artifacts
- Basic skills necessary to begin to pursue research in ML

## Requirements

### Grading

- Homework: 20%
- Projects: 40%
- Exams: 40%

Grade Cutoffs: A: 90, B: 80, C: 70, D: 60.  **No rounding**.

### Assignments

Two or three in-class written exams,  2-5 homework assignments, and a semester-long project. Your last homework or project may be due the week preceding final exams.  Assignments must be turned in before the date and time indicated as the assignment's due date.

#### Class Participation

In-class exercises cannot be made up if you do not attend the class. It’s a violation of the Academic Honor Code to submit work or sign in for other students.

#### Academic Integrity and Collaboration

We expect academic honor and integrity from students. Please study and follow the academic honor code of Georgia Tech: http://www.honor.gatech.edu/content/2/the-honor-code. You may collaborate on homework assignments, but your submissions must be your own. You may not collaborate on in-class programming exercises or exams.

#### Due Dates, Late Work, and Missed Work

* Assignments are due on the day and time listed on Canvas.  Multiple resubmissions are allowed, so submit early and often so you aren't in a rush on the due date.  **Absolutely no late submissions will be accepted.**  There is no grace period, so submit your assignments well before the deadline.

* **Make-up exams** are held at 11:00 on the Tuesday following the exam, unless otherwise announced. If the make-up exam room is not announced before the make-up day, report to the TA lab. Make-up exams are only given to students with special circumstances such as serious illness, hospitalization, death in the family, judicial procedures, military service, or official school functions. Provide us with a copy of your letter from the registrar in advance for official school functions. For other excused absences you must provide documentation to the Dean of Students's office (in the "flag" building near the ice cream cone statue) **within one week of your return from illness/activity**. The Dean of Students's office will verify your excuse and send your instructors a notice. The Dean of Student's office will also send instructors a request for flexibility in cases which don't fall within the official excused absences listed above but warrant consideration. An any case, if you believe you should be excused from a scheduled exam and don't have an excuse from the Registrar, see someone in the Dean of Students's office. Excuse from coursework or make-up opportunities are granted at the sole discretion of your instructor.

#### Regrades

To contest any grade you must submit an official regrade form to the Head TA **within one week of the assignment's original return date**. The original return date is the date the exam was first made available for students to pick up or the grade was posted online in the case of homework assignments and programming exercises. Note that a regrade means just that -- we will regrade your assignment from scratch, which means **you may end up with a lower score after the regrade**.

## Course Outline

This outline applies to Fall and Spring semesters. Summer schedule is compressed into 11 instructional weeks.

- Weeks 1 - 8: Foundations of Machine Learning
- Weeks 9 - 12: Supervised and Unsupervised Learning
- Weeks 13 - 14: Reinforcement Learning

## Prerequisites

At least one of:

- Undergraduate Semester level CS 1331 Minimum Grade of C

Unofficial:

- Linear algebra
- Probability and statistics
- Basic vector calculus

This course will include primer material on the mathematical background required for machine learning.

## Course Materials

- **Primary books**.  With the exception of *Learning from Data*, which is small and inexpensive, all of these books are available as DRM-free PDFs which can be downloaded at no cost or purchased from the web sites listed below.

    - Yaser S. Abu-Mostafa, Malik Magdon-Ismail and Hsuan-Tien Lin, *Learning from Data*,  http://amlbook.com/
        
        - The second half of the course will use e-chapters available from the http://amlbook.com/ web site. 
         
    - Miroslav Kubat, *An Introduction to Machine Learning*, https://www.springer.com/us/book/9783319639123
        
        - Georgia Tech students and faculty can download a free DRM-free PDF ebook from Springer Link though Georgia Tech's instututional subscription.  Simply visit the book web page while on the Georgia Tech network (e.g., VPN). 
    
    - Richard S. Sutton and Andrew G. Narto, *Reinforcement Learning: An Introduction*, http://incompleteideas.net/book/the-book.html

- **Recommended book**. This book is also an excellent topical introduction to machine learning that I recommend you purchase.  Unfortunately, MIT press uses DRM for its ebooks, so you'll need to decide whether you want to purchase the ebook, the physical book, or simply use other course resources.  You can get away without purchasing this book.
    
    - Ethem Alpaydin, *Introduction to Machine Learning*, https://mitpress.mit.edu/books/introduction-machine-learning-third-edition

- Deeper reading. These books go deeper than we go in this class and are typically used in more advanced machine learning courses.  These books can also be obtained as DRM-free PDFs.

    - Christopher Bishop, *Pattern Recognition and Machine Learning*, https://www.microsoft.com/en-us/research/people/cmbishop/#!prml-book -- This book is more advanced and not required for this course. Applicable lessons will cite readings from this book for students who wish to dive more deeply into the material.
    - Trevor Hastie, Robert Tibshirani and Jerome Friedman, *The Elements of Statistical Learning*, https://web.stanford.edu/~hastie/ElemStatLearn/

- Math background.  The following books provide a primer or refresher on the mathematics used in machine learning.  Some of these books can be obtained as DRM-free PDFs.

    - Marc Peter Deisenroth, A Aldo Faisal, and Cheng Soon Ong, *Mathematics for Machine Learning*, https://mml-book.com/
    - Stephen Boyd and Lieven Vandenberghe, *Introduction to Applied Linear Algebra – Vectors, Matrices, and Least Squares*, http://vmls-book.stanford.edu/
    - David Forsythe, *Probability and Statistics for Computer Science*, https://www.springer.com/us/book/9783319644097#otherversion=9783319644103


- Additional machine learning books.  These are good books which provide an additional perspective on the course material and go beyond the scope of the course.

    - Gareth James, Daniela Witten, Trevor Hastie and Robert Tibshirani, *An Introduction to Statistical Learning*, https://www-bcf.usc.edu/~gareth/ISL/
    - Martin T. Hagan, Howard B. Demuth, Mark H. Beale and Orlando De Jesus, *Neural Network Design*, http://hagan.okstate.edu/nnd.html
    - Ian Goodfellow, Yoshua Bengio and Aaron Courville, *Deep Learning*, https://www.deeplearningbook.org/
    - Dimitri P. Bertsekas, *Reinforcement Learning and Optimal Control*, http://web.mit.edu/dimitrib/www/RLbook.html
  
## Non-Discrimination

The Institute does not discriminate against individuals on the basis of race, color, religion, sex, national origin, age, disability, sexual orientation, gender identity, or veteran status in the administration of admissions policies, educational policies, employment policies, or any other Institute governed programs and activities. The Institute’s equal opportunity and non-discrimination policy applies to every member of the Institute community.

For more details see [http://www.policylibrary.gatech.edu/policy-nondiscrimination-and-affirmative-action](http://www.policylibrary.gatech.edu/policy-nondiscrimination-and-affirmative-action)
