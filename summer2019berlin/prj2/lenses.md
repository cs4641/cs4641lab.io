---
layout: homework
title: Contact Lenses
---

## Introduction

In this project, you will apply two algorithms to a contact lens data set. Answer each question in the order they appear. Do not skip to later steps to answer earlier questions that ask you to predict outcomes based on your analysis of the data and understanding of the algorithms.

## The Data Set

[UCI lenses data set]()

- [lenses.names](lenses.names)
- [lenses.data](lenses.data)

## The Algorithms

- Decision trees
- Boosted decision trees

## The Report

Write a report containing your responses to the following:

### The Contact Lens Problem

Are the features of the data numerical or categorical?

A supervised learning problem is typically either a regression problem or a classification problem.  Which kind of problem is the contact lens problem described in the data set?

Which kind of classification problem is the contact lens problem: binary or muilti-class?

How may binary classification problems can be derived from the data set?

For each binary classifcation problem you can derive from the data set, what is the minimum performance baseline?

### Decision Trees on the Contact Lens Data

Which attribute do you expect to be chosen as the split attribute at the root node?

Run a decision tree classifier on the data and report the results in a confusion matrix.

Extract a rule set from your decision tree.

### Boosting

Run boosted decision trees on the data set.

How did the boosted decision tree compare to the non-boosted decision tree?

## Tips and Considerations

- [Spark decision trees](https://spark.apache.org/docs/2.4.3/ml-classification-regression.html#decision-trees)
- Spark only supports random forests and gradient-boosted tree ensembles.  Which one is a boosting method?

- [Scikit-learn decision trees](https://scikit-learn.org/stable/modules/tree.html)
- [Scikit-learn ensembles](https://scikit-learn.org/stable/modules/ensemble.html)

## Turn-in Procedure

Submit your `lenses.pdf` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
