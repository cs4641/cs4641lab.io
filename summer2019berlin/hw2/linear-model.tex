\documentclass[answers]{exam}

\usepackage{verbatim, multicol, tabularx,}
\usepackage{amsmath,amsthm, amssymb, latexsym, stmaryrd, listings, qtree}
\usepackage{pgf,tikz}
\usetikzlibrary{arrows}
\usepackage[utf8]{inputenc}
\usepackage{listingsutf8}
\usepackage{graphicx}

\lstset{frame=tb,
language=Java,
aboveskip=-1mm,
belowskip=0mm,
showstringspaces=false,
columns=fixed, basewidth=.5em,
basicstyle={\ttfamily\small},
numbers=none,
frame=single,
breaklines=true,
breakatwhitespace=true,
inputencoding=utf8,
literate={á}{{\'a}}1 {ã}{{\~a}}1 {é}{{\'e}}1,
}

\title{Linear Model}
\date{}

\begin{document}

\maketitle

\begin{questions}

\question For each of the following, state whether you would use binary classification, multi-class classification, or regression and give a one or two sentence justification.

\begin{parts}

\part Given a mobile phone customer's account age, monthly bill, usage data and other factors, predict the likelihood that the customer will switch to another provider at the end of their current contract.

\begin{solution}
Logistic regression.  Likelihood is a probability measure, not a yes or no question.  Logistic regression computes a probability on which a yes or no answer may be given.
\end{solution}

\part Given ages, household income, and other factors, predict the number of children a couple will have.

\begin{solution}
Multi-class classification.  The number of children (or any count noun) is a categorical variable because you can't have fractional children.
\end{solution}

\part Given a traveler's answers to a series of questions, decide whether the the traveler is allowed across the bridge or not.

\begin{solution}
Binary classification.  This is a yes or no question.
\end{solution}

\part Given a college graduate's undergraduate major, institution, GPA, major GPA and other factors, predict their salary.

\begin{solution}
Linear regression.  Salary is a real number.  Linear regression predicts a real number based on real input variables (institution can be converted to a real number).
\end{solution}

\end{parts}

\newpage

\question What are the general steps of most learning algorithms?

\begin{solution}

INPUT: a data set $\mathcal{D}$ of $(\vec{x}_1, y_1), ..., (\vec{x}_n, y_n)$
\begin{itemize}

\item Initialize the parameters of a model to zeros or random values.

\item do
    \begin{itemize}
    \item For $i = 1, 2, ..., N$ for batch learning, or some $i$ for online learning
        \begin{itemize}
        \item Use current model parameters to predict $\hat{y}_i$ from $\vec{x}_i$
        \item Calculate a difference $\Delta$ between $\hat{y}_i$ and $y_i$ according to a loss function
        \item Use $\Delta$ to adjust the parameters of the model
        \end{itemize}
    \end{itemize}
   while the parameters of the model are outside some tolerance or the pre-determined number of training steps hasn't been reached
\end{itemize}

TERMINATION: the parameters of the model are "close enough" to their optimal values for the given model and data set

\end{solution}

\question What is the difference between online learning algorithms and batch learning algorithms?

\begin{solution}
Online learning algorithms update the model after seeing one training sample.
Batch learning algorithms update the model after seeing all training samples.
\end{solution}

\question How can binary classifiers be used to solve a multiclass classification problem?

\begin{solution}
Some classifiers can do multi-class classification (e.g., multinomial logistic regression).

Binary classifiers can be combined in a chain to handle multiclass problems where the first classifier predicts whether an instance is in $C_1$, if not the second classfier predicts whether an instance is in $C_2$, and so on for classes $C_1, ..., C_n$.
\end{solution}

\question How can linear models deal with data sets that are not linearly separable due to noise in the data?

\begin{solution}
In the case of noise in the data set, we can minimize some error or loss function to get a model that is "good enough" rather than insisting on a perfect fit to the data.

In the case of fundamentally non-linearly separable data we can use a nonlinear transformation to project the instances of the data set into a space, a $\mathcal{Z}$-space, in which they are linearly separable.
\end{solution}

\newpage

\question What is the form of the hypothesis class of linear classifiers?

\begin{solution}
\[
    \mathcal{H} = \{h(\vec{x}) = sign(\vec{w}^T \cdot \vec{x})\},
\]

where
\[
\vec{w} =
\begin{bmatrix}
w_{0} \\
w_{1} \\
\vdots \\
w_{d}
\end{bmatrix}
\in \mathbb{R}^{d + 1}
\]

and

\[
\vec{x} =
\begin{bmatrix}
1 \\
x_{1} \\
\vdots \\
x_{d}
\end{bmatrix}
\in \{1\} \times \mathbb{R}^d
\]

and

\begin{itemize}
\item $\vec{w} \in \mathbb{R}^{d + 1}$ where $d$ is the dimensionality of the input space and $w_0$ is a bias weight, and
\item $x_0 = 1$ is fixed.
\end{itemize}

\end{solution}

\question The linear signal is $s = \vec{w}^T\vec{x}$.  What are the prediction functions, $\theta(s)$, and the error/loss functions for each of the following:

\begin{parts}

\part the perceptron

\begin{solution}
Prediction function:
\[
    h(\vec{x}) = sign(\vec{w}^T\vec{x})
\]

0-1 loss, which is simply the classification error:
\[
    e(h(\vec{x}), f(\vec{x})) = \llbracket h(\vec{x}) \ne f(\vec{x}) \rrbracket
\]
for pointwise error, or
\[
    E(h) = \frac{1}{N} \sum_{n=1}^{N}  \llbracket h(\vec{x}) \ne f(\vec{x}) \rrbracket
\]
over the data set.
\end{solution}

\part linear regression

\begin{solution}
Prediction function:
\[
    h(\vec{x}) = \vec{w}^T\vec{x}
\]

Mean-square error (MSE): $E_{in} = \frac{1}{N} \sum_{n=1}^{N} ( h(\vec{x_n} - y_n )^2$
\end{solution}

\part logistic regression

\begin{solution}
Prediction function: logistic sigmoid
\[
    h(\vec{x}) = \theta(\vec{w}^T\vec{x})
\]
where
\[
    \theta(s) = \frac{e^s}{1+e^s}
\]

Cross-entropy error:
\[
E_{in}(\vec{w}) = \sum_{n=1}^{N} \llbracket y_n = +1 \rrbracket \ln \frac{1}{h(\vec{x}_n)} + \llbracket y_n = -1 \rrbracket \ln \frac{1}{1 - h(\vec{x}_n)}
\]
\end{solution}

\end{parts}

\question What is a local minimum? What is the global minimum?

\begin{solution}
A local minimum is a value of a function $f(x)$ for which the neighboring values $f(x - \delta)$ and $f(x + \delta)$ are greater than $f(x)$ for small $\delta$ but there are other values of $f(x)$ that are lower.  This phenomenon is depicted below by a function with many "valleys", only one of which is the lowest valley (the global miniumum) and the others are local minima.

\includegraphics[height=2in]{../../slides/lfd-local-minima.png}
\end{solution}

\question What is the meaning of the learning rate hyperparameter to a learning algorithm?

\begin{solution}
The learning rate specifies the magnitude of model parameter corrections applied in each step of a learning algorithm.
\end{solution}

\newpage

\question What may happen if the learning rate is set too low?

\begin{solution}
Learning may take too long to converge.

\includegraphics[height=2in]{../../slides/lfd-small-learning-rate.png}
\end{solution}

\question What may happen if the learning rate is set too high?

\begin{solution}
Parameters may be adjusted too much so that they "bounce around" the global minimum.

\includegraphics[height=2in]{../../slides/lfd-large-learning-rate.png}
\end{solution}

\question Describe two desirable features of the sigmoid loss function for logistic regression.

\begin{solution}
The sigmoid loss function is {\it differentiable}, which means it can be minimized with gradient descent and it is {\it convex}, which means there is a single minimum, which is the global minimum.

\includegraphics[height=2in]{../../slides/lfd-convex-error.png}

\end{solution}

\question Can a linear model be used to separate into classes the feature vectors of instances that are not linearly  separable?  If so, how?

\begin{solution}
Yes, by projecting the instances in the data set into a space in which they are linearly separable using a feature transform $\Phi(\vec{x})$.

\end{solution}

\question What happens to sample complexity (the number of training samples we need to maintain a bound on generalization  error) with higher-order polynomial transforms?

\begin{solution}
Transforming data to higher-dimensional spaces increases the sample complexity because it effectively increases the VC dimension of models that are able to separate the data in the $mathcal{Z}$ space.
\end{solution}

\end{questions}

\end{document}
