---
layout: default
title: CS4641 - Resources
---

# Resources

## Software

>NOTE: At many points in these instructions you are told to open the "Command Prompt"/"Command Line" (Windows) or "Terminal" Mac OS X/Linux. You will be using this program a lot so you should get familiar with it.  You may want to pin it to your taskbar/dock.
> - On Mac open the Applications folder, then the Utilities folder, then open Terminal.app.
> - On Linux the Terminal can by opened with the keyboard shortcut "ctrl-alt-t".
> - On Windows open the start menu and search for "cmd" then click on the Command Prompt.

Machine learning systems can be developed with variety of technology stacks.  The choice of technology stack onvolves trade-offs between scalability, usability, and flexibility.  Of course many people simply choose a stack that's built on a language they already know. In this course you may choose from the following widely-used technology stacks:

- [Spark](spark.html) (Although you may use Scala, Python, or R with Spark, Scala is the main language, so in this course we consider Spark a Scala-based technology stack.)
- [Python/Scipy](scipy.html)
- [R](r.html)
- [Julia](julia.html)

## Data

Data is the lifeblood of machine learning.  We will provide you with data sets for your homework projects, but there are many more data sets available for exploration.

- [UCI Machine Learning Repository](http://archive.ics.uci.edu/ml/index.php)


## Help

- If you have general questions about course content or homework clarifications please post your questions on our <a href="http://piazza.com/gatech/" target="_blank">Piazza site</a>. Be sure not to post homework code for other students to see and **don't post screen shots of text** -- just copy an d paste the text.
- Visit the TA Lab in CoC! TAs hold [office hours](office-hours.html) there to answer questions and address any concerns you may have. Also, the TA Lab will be your resource for picking up tests you missed in recitation, submitting regrade requests etc.
- If you find broken links on slides or lecture notes you can look at <a href="https://gitlab.com/cs4641/cs4641.gitlab.io" target="_blank">the GitLab repo of this class web site</a>.  You can also find the example projects we discuss in class on <a href="https://gitlab.com/cs4641/" target="_blank">the CS4641 GitLab group page.</a>

## General Computing

- <a href="http://matt.might.net/articles/basic-unix/" target="_blank">Basic Unix</a> - a tutorial introduction to the Unix command line that will give you the basic skills you need for this class should you choose to use a unix operating system like Linux or macOS
- <a href="https://www.computerhope.com/issues/chusedos.htm" target="_blank">Windows command line tutorial</a>
- <a href="https://technet.microsoft.com/en-us/library/bb490890.aspx" target="_blank">Windows command line reference</a>
- [Customization Tips](customization-tips.html) for the bash shell, Atom text editor, and Sublime Text editor.
