---
layout: homework
title: hw1 - Machine Learning Problems
---

# Machine Learning Problems

According to Tom Mitchell, machine learning is the study of algorithms that

- improve their performance `P`
- at some task `T`
- with experience `E`.

A well-defined learning task is given by `<P, T, E>`.

## Learning From Data Exercise 1.1

Formulate the problems in Learning From Data Exercise 1.1. as machine learning problems according to Tom Mitchell's machine learning problem specification and the specification in Learning From Data.  So for each problem you'll specify:

- The task `T`,
- The performance measure `P`,
- The experience `E`,
- The target function $$f: \mathcal{X} \rightarrow \mathcal{Y}$$, that is,

    - the input space $$\mathcal{X}$$, and
    - the output space $$\mathcal{Y}$$.

Remember that a function maps a domain to a co-domain, and these domains are sets.

## Turn-in Procedure

Write up your homework in a format that can be converted to a PDF file (like LaTeX) and name the PDF file `ml-problems.pdf`.  Submit your `ml-problems.pdf` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
