#!/usr/bin/env python

import argparse
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import sys

def create_data_plot(data):
    fig, ax = plt.subplots(1, 1)
    ax.set_xlim(left = 0, right = np.max(data["age"])+10)
    ax.set_ylim(bottom = 0, top = np.max(data["income"])+10)
    ax.set_xlabel("age")
    ax.set_ylabel("income")
    ax.scatter(x=data[data["approve"] == 1]["age"],
               y=data[data["approve"] == 1]["income"], marker="+", color="green")
    ax.scatter(x=data[data["approve"] == -1]["age"],
               y=data[data["approve"] == -1]["income"], marker="_", color="red")
    return fig, ax

def plot_separator(data, ws, i, mislabelled=np.array([0])):
    fig, ax = create_data_plot(data)
    ax.set_title(f"Separator after {i} iterations")
    y_intercept = -ws[0]/ws[2]
    x_intercept = -ws[0]/ws[1]
    print(f"x_intercept = {x_intercept:.1f}")
    print(f"y_intercept = {y_intercept:.1f}")
    if len(mislabelled) > 1:
        circle = plt.Circle((mislabelled[0], mislabelled[1]), radius=5, color='yellow', fill=False)
        #ax.scatter([mislabelled[0]], [mislabelled[1]], marker="o", color="yellow")
        ax.add_artist(circle)
    ax.plot([0, x_intercept], [y_intercept, 0], "-b")
    fig.savefig(f"./animation/credit-pla_{i:05d}.png")

def pick_mislabelled(X, ys, ws):
    # predicted_ys = np.array([np.sign(x.dot(ws)) for x in X])
    predicted_ys = np.sign(X.dot(ws)).astype(int)
    print("ys: ", ys)
    print("^ys:", predicted_ys)
    for i in range(len(ys)):
        if predicted_ys[i] != ys[i]:
            print(f"Mislabelled X[{i}]: {X[i]}, label: {ys[i]}, prediction: {predicted_ys[i]}")
            errors_present = True
            break
    else:
        # If we didn't break out of the loop, we didn't find any errors
        errors_present = False
    return errors_present, i

def pla(data):
    #fig, ax = create_data_plot(data)
    label_column = data.columns[-1]
    X = data.drop(label_column, axis=1).to_numpy()
    n = X.shape[0]
    dim = X.shape[1]
    X_augmented = np.hstack((np.ones((n, 1)), X))
    ys = data[label_column].to_numpy()
    #ws = np.random.random(dim + 1)
    ws = np.zeros(dim + 1)
    print("Initial ws:", ws)
    errors_present = True
    iteration = 1
    while errors_present:
        print(f"\nIteration {iteration}")
        errors_present, mislabelled = pick_mislabelled(X_augmented, ys, ws)
        if errors_present:
            ws = ws + (ys[mislabelled] * X_augmented[mislabelled])
            print(f"ws:", ws)
            #if iteration == 10: break
            #print(f"\rIteration {iteration}", end="")
            if iteration % 10 == 0:
                plot_separator(data, ws, iteration, X[mislabelled])
            iteration = iteration + 1
    plot_separator(data, ws, iteration)
    print(f"\nFinal weights: {ws}")
    predicted_ys = np.sign(X_augmented.dot(ws)).astype(int)
    print("Final predicted ys:", predicted_ys)
    print("ys:                ", ys)


def make_argparser():
    parser = argparse.ArgumentParser(description='Run perceptron learning algorithm on data set')
    parser.add_argument("-d", "--data", dest="data", required=True,
                        help="The name of the CSV data file with header row.  Last column is label, 1, or -1")
    return parser

def main(argv):
    parser = make_argparser()
    args = parser.parse_args(argv[1:])
    data = pd.read_csv(args.data)
    pla(data)

if __name__=="__main__":
    main(sys.argv)
