
def pacLearnability(errorTolerance: Double, confidence: Double, modelSize: Int): Int = {
  val delta = 1 - confidence
  ((1 / errorTolerance) * (math.log(modelSize) + math.log(1 / delta))).toInt
}

pacLearnability(.1, .9, math.pow(3, 4).toInt)

// Equation 2.13 on Page 57 of Learning From Data
// The reason for putting n in a second parameter list
// will become clear when we write a general fixed-point
// finder.
def vcComplexity(dvc: Int,
                 epsilon: Double,
                 delta: Double)
                (n: Int): Int = {
  ((8 / math.pow(epsilon, 2)) *
    (math.log((4 * (math.pow((2 * n), dvc) + 1) /
      delta)))).toInt
}

val dvc = 3
val errorTolerance = 0.1
val confidence = 0.1

var n: Int = 1000
for (i <- 1 to 10) {
  n = vcComplexity(dvc, errorTolerance, confidence)(n)
  println(n)
}

// Functional approach
(1 to 10).foldLeft(1)((n1: Int, throwAway: Int) =>
  vcComplexity(dvc, errorTolerance, confidence)(n1))

// We can also write a fixed-point finder:
def fixedPoint[A](init: A, f: A => A): A = {
  var previousResult = f(init)
  var newResult = f(previousResult)
  while (newResult != previousResult) {
    previousResult = newResult
    newResult = f(previousResult)
  }
  newResult
}

// Then curry the vcComplexity function:
val vccf = vcComplexity(dvc, errorTolerance, confidence) _

// And feed the curried vcComplexity function to our
// fixed-point finder:
fixedPoint(1000, vccf)

// What does N look like for different VC dimensions?
val vcVersusN = for {
  dvc <- 1 to 10
  vccf = vcComplexity(dvc, errorTolerance, confidence) _
} yield (dvc, fixedPoint(1, vccf))

vcVersusN.foreach(t =>
  println(s"For dvc = ${t._1}, N = ${t._2}"))

def vcBound(delta: Double, n: Int, dvc: Int): Double = {
  (8 / n.toDouble) *
    (math.log((4 * (math.pow((2 * n), dvc) + 1)) /
      delta))
}

val dvcVersusError = for {
  dvc <- 1 to 10
  vcb = vcBound(confidence, 1000, dvc)
} yield (dvc, vcb)

println("For N = 1000")
dvcVersusError.foreach(t =>
  println(s"If dvc = ${t._1}, error bound = ${t._2}"))
