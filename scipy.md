---
layout: default
title: Scipy
---

# Python/Scipy

## Installation

- **Recommended**: [Anaconda](https://www.anaconda.com/distribution/) has everything you need, and is used by many courses you'll take after this course.  Be sure to get the 64-bit Python 3 version for your operating system.

- Minimalist: [Miniconda](http://conda.pydata.org/miniconda.html). Be sure to get the Python 3 version! Once you have Miniconda, open your terminal application (or `cmd` on Windows), and enter the following commands (`$` is the command prompt on most Unix shells. It would be something like `C:\>` in Windows's terminal, cmd. You don't type/copy the prompt from shell examples like this.):

```sh
$ conda update conda
$ conda install ipython numpy pandas matplotlib jupyter statsmodels scipy scikit-learn
```

> Note: Miniconda is the pakage manager part of the Anaconda distribution.  If you already have Anaconda you can install Anaconda with `conda install anaconda`.  Even if you have the full Anaconda distribution you can use Miniconda from the command line.


### Check Your Python Installation


1. Open a terminal, i.e., operating system (OS) command shell.

    -  The Mac OS X terminal is `/Applications/Utilities/Terminal.app` in the Finder.  You should drag Terminal to your dock -- you'll use it often.

    - On Windows search for and run `cmd.exe`, for example by simply typing `cmd` in the start search box.

2. At the command prompt type `python --version`.  You should get a response like `Python 3.5.2 :: Continuum Analytics, Inc.`.

### Choose, install and configure a text editor using our [text editors guide](https://datamastery.gitlab.io/computing/text-editors.html).

## Python Resources

- Online Python documentation: [https://docs.python.org/3/](https://docs.python.org/3/)
- [Python Style Guide](http://legacy.python.org/dev/peps/pep-0008/)
- [Anaconda Documentation](https://docs.anaconda.com/anaconda/)
- Jay Summet's previous [CS 2316 class web sites](http://www.cc.gatech.edu/~summetj/teaching.html#cs2316)

## Enrolled in Modern Physics 2?

If you're enrolled in Modern Physics 2 and need to run VPython for Physics, then you'll need to manage a Python 2 installation for Physics and Python3 for this course. You can manage two Python version using [conda environments](http://conda.pydata.org/docs/using/envs.html). To set it up, do this at the command line:

```sh
conda create -n py27 python=2.7
conda install psutil
source activate py27
conda install -c mwcraig vpython=6.11.0
```

The first two lines create a conda environment for Python 2.7, the third line activates it, and the fourth line installs vpython into your Python 2.7 environment (named py27).

With this setup, any time you want to run vpython you have to do

```sh
source activate p27
```

And when you're done you can go back to the root Python 3 environment for this course with

```sh
source deactivate
```

### Non-Conda Solution

Some students have reported success installing the Physics VPython software after installing Python 2 from [python.org](https://www.python.org/). With this setup you would use `python` for Physics and `python3` for this course. At the command line you should get something like this:

```sh
$ python --version
Python 2.7.3
$ python3 --version
Python 3.5.2 :: Continuum Analytics, Inc.
```
